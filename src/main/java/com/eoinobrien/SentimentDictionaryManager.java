package com.eoinobrien;

import com.eoinobrien.lexiconsHandlers.BingLiuHandler;
import com.eoinobrien.lexiconsHandlers.EmojiHandler;
import com.eoinobrien.lexiconsHandlers.MPQAHandler;
import com.eoinobrien.lexiconsHandlers.SentiWordNetHandler;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Eoin O'Brien on 21/12/15.
 */

public class SentimentDictionaryManager {
    private static final Logger logger = LoggerFactory.getLogger(SentimentDictionaryManager.class);

    public static void main(String[] args) {
        if (args.length != 1) {
            logger.error("Config file not passed as args. Exiting.");
            System.exit(0);
        }

        try {
            Config config = new Config(args[0]);
            Map<String, DictionaryEntry> dictionary = new HashMap<>();

            String sentiWord_fileName = config.getString("lexicon.sentiWordNet");
            if (sentiWord_fileName != null && !sentiWord_fileName.equals("")) {
                SentiWordNetHandler sentiWordNetHandler = new SentiWordNetHandler(sentiWord_fileName);
                mapToDictionary(sentiWordNetHandler.getSentimentDictionary(), dictionary);
            }

            String mpqa_fileName = config.getString("lexicon.mpqa");
            if (mpqa_fileName != null && !mpqa_fileName.equals("")) {
                MPQAHandler mpqaHandler = new MPQAHandler(mpqa_fileName);
                mapToDictionary(mpqaHandler.getSentimentDictionary(), dictionary);
            }

            BingLiuHandler bingLiuHandler;
            String bingPos_fileName = config.getString("lexicon.bingLiu.positive");
            if (bingPos_fileName != null && !bingPos_fileName.equals("")) {
                bingLiuHandler = new BingLiuHandler(bingPos_fileName, true);
                mapToDictionary(bingLiuHandler.getSentimentDictionary(), dictionary);
            }
            String bingNeg_fileName = config.getString("lexicon.bingLiu.negative");
            if (bingNeg_fileName != null && !bingNeg_fileName.equals("")) {
                bingLiuHandler = new BingLiuHandler(bingNeg_fileName, false);
                mapToDictionary(bingLiuHandler.getSentimentDictionary(), dictionary);
            }

            String emoji_fileName = config.getString("lexicon.emoji");
            if (emoji_fileName != null && !emoji_fileName.equals("")) {
                EmojiHandler emojiHandler = new EmojiHandler(emoji_fileName);
                mapToDictionary(emojiHandler.getSentimentDictionary(), dictionary);
            }

            Settings settings = ImmutableSettings.settingsBuilder()
                    .put("cluster.name", config.getString("cluster.name")).build();
            Client client = new TransportClient(settings)
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(config.getString("host.name")), config.getInt("host.port")));

            BulkRequestBuilder bulkRequest = client.prepareBulk();
            int count = 0;
            System.out.println("\nSize: " + dictionary.size());
            for (Map.Entry<String, DictionaryEntry> entry : dictionary.entrySet()) {
                bulkRequest.add(client.prepareIndex(config.getString("lexicon.index"), config.getString("lexicon.type"), entry.getKey())
                        .setSource(entry.getValue().getESJsonObject()));

                count++;
                if (count % 5000 == 0 || dictionary.size() == count) {
                    //Splitting requests to prevent running out of memory
                    BulkResponse bulkResponse = bulkRequest.get();
                    if (bulkResponse.hasFailures()) {
                        bulkResponse.buildFailureMessage();
                    }
                    bulkRequest = client.prepareBulk();
                    System.out.println("Completed: " + count);
                }
            }

            client.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void mapToDictionary(Map<String, Double> inputMap, Map<String, DictionaryEntry> dictionary) {
        for (Map.Entry<String, Double> entry : inputMap.entrySet()) {
            String key = entry.getKey();
            if (!dictionary.containsKey(key)) {
                dictionary.put(key, new DictionaryEntry(key, entry.getValue()));
            } else {
                DictionaryEntry dictionaryEntry = dictionary.get(key);
                dictionaryEntry.addSentimentValue(entry.getValue());
                dictionary.put(key, dictionaryEntry);
            }
        }
    }
}
