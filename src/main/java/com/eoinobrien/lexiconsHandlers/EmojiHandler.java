package com.eoinobrien.lexiconsHandlers;

import com.vdurmont.emoji.EmojiParser;
import java.util.AbstractMap;

/**
 * Created by eoin on 16/02/16.
 */
public class EmojiHandler extends Handler {

    public EmojiHandler(String fileName) {
        super(fileName);
    }

    public Object getValues(String line) {
        if(line.length() == 0 || line.substring(0,line.indexOf(',')).equals("Emoji")) {
            return null;
        }

        String[] columns = line.split(",");

        String unicode = columns[1];
        unicode = "&#" + unicode.substring(1) + ";";
        double sentiment = Double.parseDouble(columns[5]);
        String alias = EmojiParser.parseToUnicode(unicode);
        alias = EmojiParser.parseToAliases(alias);

        if(alias.indexOf(":") == 0) {
            return new AbstractMap.SimpleEntry<>(alias, sentiment);
        }

        return null;
    }
}
